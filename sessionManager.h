#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H

#include <QNetworkConfiguration>
#include <QNetworkConfigurationManager>
#include <QNetworkSession>
#include <QObject>

#include "sessionHandler.h"

class SessionManager : public QNetworkConfigurationManager {
  Q_OBJECT

public:
  SessionManager();

signals:
  void connectionStateUpdated(QString);

private slots:
  void monitorSession(QNetworkConfiguration) ;
  void unmonitorSession(QNetworkConfiguration) ;
  void emitStateUpdated(QString);

private:
  QMap<QString,SessionHandler *> m_sessions;
};

#endif // SESSIONMANAGER_H
