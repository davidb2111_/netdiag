#include <QDebug>
#include <QMutex>
#include <QTimer>

#include "diagnose.h"
#include "netdiagDns.h"
#include "netdiagGateway.h"
#include "netdiagInterfaces.h"
#include "poller.h"
#include "preferences.h"
#include "states.h"

Poller::Poller() :
  Diagnose() {
  
  // create instances of different modules
  //update();
  mutex = new QMutex();
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(update()));
  setInterval(5000); // by default, 5 secondes polling interval
  timer->start();
}

void Poller::update() {
  if (mutex->tryLock()) {
    if (checkInterval != currentCheckInterval) {
      currentCheckInterval = checkInterval;
      timer->setInterval(currentCheckInterval*1000);
    }
    start();
    emit updated(getState(), getModuleMessage(), getErrorMessage());
    mutex->unlock();
  }
}

void Poller::setInterval(int interval) {
  qDebug() << "new polling interval" << interval*1000;
  checkInterval = interval;
}

