#ifndef NETDIAGDNS_H
#define NETDIAGDNS_H

#include <QObject>
#include <QtNetwork/QHostInfo>


// this class test gateway reachability

class NetdiagDns : public QObject {
  Q_OBJECT

public:
  NetdiagDns();
  void prepare();
  bool diagnose();
  void setHostname(QString);
  QString getReason();
signals:
  void resolved();
private slots:
  void lookedUp(const QHostInfo &);
private:
  QString m_hostname;
  QString m_reason;
  bool m_result; // test result
};

#endif // NETDIAGDNS_H
