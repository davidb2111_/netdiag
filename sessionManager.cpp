#include <QNetworkConfigurationManager>
#include <QNetworkSession>

#include "sessionManager.h"
#include "sessionHandler.h"

SessionManager::SessionManager()
  :QNetworkConfigurationManager() {
  for(QNetworkConfiguration networkConfig : allConfigurations()) {
    monitorSession(networkConfig);
  }
  connect(this, SIGNAL(configurationAdded(QNetworkConfiguration)), this, SLOT(monitorSession(QNetworkConfiguration)));
  connect(this, SIGNAL(configurationRemoved(QNetworkConfiguration)), this, SLOT(unmonitorSession(QNetworkConfiguration)));
}

void SessionManager::monitorSession(QNetworkConfiguration connectionConfig) {
  SessionHandler * session = new SessionHandler(connectionConfig);
  m_sessions.insert(connectionConfig.identifier(), session);
  connect(session, SIGNAL(networkConfigurationStateChanged(QString)), this, SLOT(emitStateUpdated(QString)));
}

void SessionManager::unmonitorSession(QNetworkConfiguration connectionConfig) {
  disconnect(m_sessions[connectionConfig.identifier()], SIGNAL(networkConfigurationStateChanged(QString)), this, SLOT(emitStateUpdated(QString)));
  m_sessions[connectionConfig.identifier()]->cleanup();
  m_sessions.remove(connectionConfig.identifier());
}

void SessionManager::emitStateUpdated(QString connectionName) {
  qDebug() << "connection updated" << connectionName;
  emit connectionStateUpdated(connectionName);
}

