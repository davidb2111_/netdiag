#include <QApplication>
#include "diagnose.h"
#include "netdiagInterfaces.h"
#include "netdiagGateway.h"
#include "netdiagDns.h"
#include "netdiagRoutes.h"
#include "netdiagCaptivePortal.h"
#include "netdiagInternet.h"
#include "preferences.h"
#include "states.h"


Diagnose::Diagnose() {
  state = STATE_OK;
  setModuleMessage("Interface", tr("Interface test pending"));
  setModuleMessage("Gateway", tr("Gateway test pending"));
  setModuleMessage("DNS", tr("DNS test pending"));
  setModuleMessage("Captive", tr("Internet test pending"));
  setModuleMessage("Internet", tr("Internet test pending"));
}

QList<DiagnoseResult*> Diagnose::doDiag() {
  qDebug() << "Diagnose::doDiag()";
  NetdiagInterfaces interfaces;
  NetdiagRoutes routes;
  NetdiagDns dns;
  NetdiagCaptivePortal captive;
  NetdiagInternet internet;
  QList<DiagnoseResult*> result;
  bool res;

  // 1st local interfaces, check global scope address availability
  interfaces.prepare();
  res = interfaces.diagnose();
  result.append(new DiagnoseResult(Diagnose::INTERFACE, interfaces.getReason(), res ? STATE_OK : STATE_EINTERFACE));
  emit testComplete(Diagnose::INTERFACE, interfaces.getReason(), res ? STATE_OK : STATE_EINTERFACE);

  // 2nd routes
  for(NetdiagGateway *gateway : interfaces.getDefaultGatewayList()) 
    routes.addRoute(gateway);

  res = routes.diagnose();
  result.append(new DiagnoseResult(Diagnose::ROUTING, routes.getReason(), res ? STATE_OK : STATE_EGATEWAY));
  emit testComplete(Diagnose::ROUTING, routes.getReason(), res ? STATE_OK : STATE_EGATEWAY);

  // 3nd dns
  dns.prepare();
  dns.setHostname(Preferences::dnsHostname());
  res = dns.diagnose();
  result.append(new DiagnoseResult(Diagnose::DNS, dns.getReason(), res ? STATE_OK : STATE_EDNS));
  emit testComplete(Diagnose::DNS, dns.getReason(), res ? STATE_OK : STATE_EDNS);

  // 4th internet
  captive.prepare();
  int captiveResult;
  switch (captive.diagnose()) {
    case NetdiagCaptivePortal::PORTALDETECTED:
      captiveResult = STATE_ECAPTIVE;
      emit portalTrigger(true);
      break;
    case NetdiagCaptivePortal::NOPORTAL:
      captiveResult = STATE_OK;
      emit portalTrigger(false);
      break;
    default: // captive portal test returned an error, assume no portal
      captiveResult = STATE_OK;
      emit portalTrigger(false);
  }
  result.append(new DiagnoseResult(Diagnose::CAPTIVE, captive.getReason(), captiveResult));
  emit testComplete(Diagnose::CAPTIVE, captive.getReason(), captiveResult);

  // 5th internet
  internet.prepare();
  res = internet.diagnose();
  result.append(new DiagnoseResult(Diagnose::INTERNET, internet.getReason(), res ? STATE_OK : STATE_EINTERNET));
  emit testComplete(Diagnose::INTERNET, internet.getReason(), res ? STATE_OK : STATE_EINTERNET);
  return(result);
}

void Diagnose::start() {
  QList<DiagnoseResult*> diagResult = doDiag();
  for(int i=0; i < diagResult.size(); i++) {
    qDebug("%.20s %s", qUtf8Printable(stepToString(diagResult[i]->module) + "...................."), qUtf8Printable(diagResult[i]->message));
    setModuleMessage(stepToString(diagResult[i]->module), diagResult[i]->message);
    if (diagResult[i]->status != STATE_OK) {
      qDebug() << diagResult[i]->module << diagResult[i]->message;
      setErrorState("Netindic", diagResult[i]->message, diagResult[i]->status);
      return;
    }
  }
  setErrorState("Netindic", tr("Full network connectivity"), STATE_OK);
}


QString Diagnose::stepToString(int i) {
  switch(i) {
    case INTERFACE:
      return tr("Interface");
      break;
    case ROUTING:
      return tr("Routing");
      break;
    case DNS:
      return tr("DNS");
      break;
    case CAPTIVE:
      return tr("Captive portal");
      break;
    case INTERNET:
      return tr("Internet");
      break;
    default:
      return tr("Unknown");
  }
}


bool Diagnose::hasFailed() {
  if (state)
    return true ;
  return false;
}

void Diagnose::setErrorState(QString module, QString message, int st) {
  setModuleMessage(module, message);
  moduleMessage = module;
  errorMessage = message;
  state = st;
}

void Diagnose::setModuleMessage(QString module, QString message) {
  reasons[module] = message;
}

QString Diagnose::getModuleMessage() {
  return moduleMessage;
}

QString Diagnose::getErrorMessage() {
  return errorMessage;
}

int Diagnose::getState() {
  return state;
}

QString Diagnose::getInterfaceStatus() {
  return reasons["Interface"];
}

QString Diagnose::getGatewayStatus() {
  return reasons["Gateway"];
}

QString Diagnose::getDnsStatus() {
  return reasons["DNS"];
}

QString Diagnose::getCaptiveStatus() {
  return reasons["Captive"];
}

QString Diagnose::getInternetStatus() {
  return reasons["Internet"];
}


