#include <QFileInfo>
#include <QString>
#include <QStringList>

static QStringList defaultAssetsSearchPaths = QStringList() 
  << "/usr/share/netindic/static/"
  << "./static/"
  << "./";

// return path to asset files
QString assetsPathFinder(QStringList additionalPaths = {}) {
  for(QString p : defaultAssetsSearchPaths) {
    if (QFileInfo::exists(p) && QFileInfo(p + "default/local.png").isFile())
      return p;
  } 

  for(QString p : additionalPaths) {
    if (QFileInfo::exists(p) && QFileInfo(p + "default/local.png").isFile())
      return p;
  }

  return nullptr;
}
