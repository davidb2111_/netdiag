#ifndef CONFIG_H
#define CONFIG_H

#define env_coalesce(a, b) (std::getenv(a) != NULL ? std::getenv(a) : b)
#define CMD_IPV4_ROUTE env_coalesce("CMD_IPV4_ROUTE", "ip -4 route show table main")
#define CMD_IPV6_ROUTE env_coalesce("CMD_IPV6_ROUTE", "ip -6 route show table main")

#endif
