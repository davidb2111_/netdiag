#ifndef STATES_H
#define STATES_H

#define STATE_OK         0
#define STATE_EINTERFACE 1
#define STATE_EGATEWAY   2
#define STATE_EDNS       3
#define STATE_EINTERNET  4
#define STATE_ECAPTIVE   5

#endif // STATES_H
