#ifndef NETDIAGCAPTIVEPORTAL_H
#define NETDIAGCAPTIVEPORTAL_H

#include <QObject>
#include <QNetworkReply>
#include <QtNetwork/QHostInfo>
#include <QWidget>


// this class test gateway reachability

class NetdiagCaptivePortal : public QObject {
  Q_OBJECT

public:
  enum {
    NOPORTAL, PORTALDETECTED, PORTALDETECTIONERROR
  };
  NetdiagCaptivePortal();
  void prepare();
  int diagnose();
  QString getReason();
  void setUrl(QString);

signals:

private slots:

private:
  QNetworkReply * get();

  QWidget window;
  QString m_url;
  QString m_reason;
  bool m_result; // test result
};

#endif // NETDIAGINTERNET_H
