#ifndef PINGMODEL_H
#define PINGMODEL_H

#include <QObject>
#include <QProcess>
#include <QtNetwork/QHostAddress>

class PingModel : public QObject
{
    Q_OBJECT
public:
    explicit PingModel(QObject *parent = 0);
    ~PingModel();
    void setHost(QHostAddress);
    void setHost(QHostAddress, QString dev);
    void run();
    bool isSuccess();
    bool finished();
    float getRtt() ;

signals:

public slots:
    void readResult();

private:
    QProcess *ping;
    bool success;
    QHostAddress host;
    float rtt;

};

#endif // PINGMODEL_H
