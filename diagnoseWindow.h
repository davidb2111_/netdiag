#ifndef DIAGNOSEWINDOW_H
#define DIAGNOSEWINDOW_H

#include <QDialog>
#include <QLabel>
#include <QThread>
#include <QTimer>

#include "diagnose.h"

namespace Ui {
class DiagnoseWindow;
}

class DiagnoseWindow : public QDialog {
  Q_OBJECT
  QThread workerThread;
public:
  explicit DiagnoseWindow(QWidget *parent = 0);
  ~DiagnoseWindow();
  void open();
protected:
  void closeEvent(QCloseEvent*);

private slots:
  void refreshWindow(int, QString, int);

private:
  void close();

  QWidget window;
  QLabel * l_interfaceText;
  QLabel * l_gatewayText;
  QLabel * l_dnsText;
  QLabel * l_captiveText;
  QLabel * l_internetText;
  Diagnose * diagnose;
  QTimer * timer;
};

#endif // DIAGNOSEWINDOW_H
