#ifndef NETDIAGINTERNET_H
#define NETDIAGINTERNET_H

#include <QObject>
#include <QtNetwork/QHostInfo>


// this class test gateway reachability

class NetdiagInternet : public QObject {
  Q_OBJECT

public:
  NetdiagInternet();
  void prepare();
  bool diagnose();
  QString getReason();
  void setUrl(QString);

signals:

private slots:

private:
  bool get();

  QString m_url;
  QString m_reason;
  bool m_result; // test result
};

#endif // NETDIAGINTERNET_H
