#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTimer>
#include <QEventLoop>
#include "netdiagInternet.h"
#include "preferences.h"

NetdiagInternet::NetdiagInternet() {
  prepare();
}

void NetdiagInternet::prepare() {
  m_url = Preferences::internetTestUrl();
  m_reason = tr("test pending");
}

void NetdiagInternet::setUrl(QString url) {
  m_url = url;
}

bool NetdiagInternet::diagnose() {
  for(uint32_t i = 0; i < Preferences::internetMaxAttempts(); i++)
    if (get()) {
      m_reason = tr("internet reachable");
      return true;
    }
  return false;
}

bool NetdiagInternet::get() {
  QEventLoop loop;
  QNetworkReply * reply;
  QNetworkAccessManager * manager = new QNetworkAccessManager(this);
  QTimer timer;
  timer.setSingleShot(true);

  connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

  reply = manager->get(QNetworkRequest(m_url));

  connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  timer.start(Preferences::internetTimeout() * 1000);
  loop.exec(); // wait for either manager() or timer() to emit signal
  if (timer.isActive()) { // lookup signal received
    if (reply->error() != QNetworkReply::NoError) {
      m_reason = tr("get reply error: %1").arg(reply->error()) ;
      return false;
    }
    if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() != 200) {
      m_reason = tr("get return an error: %1").arg(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString());
      return false;
    }
    return true; // test passed
  }else { // 
    m_reason = tr("Can't get %1: timeout").arg(m_url);
    reply->abort();
    //QHostInfo::abortHostLookup(id);
    return false;
  }

}

QString NetdiagInternet::getReason() {
  return m_reason;
}

