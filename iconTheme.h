#ifndef ICONTHEME_H
#define ICONTHEME_H

#include <QObject>
#include <QPixmap>

#include "states.h"

class IconTheme : public QObject {
  Q_OBJECT

public:
  IconTheme(QString);
  QPixmap getStatePixmap(int state);
  QPixmap getInterfacePixmap();
  QPixmap getGatewayPixmap();
  QPixmap getInternetPixmap();
  QPixmap getConnectionPixmap();

private:
  QString m_theme;
  QPixmap m_stateOk;
  QPixmap m_stateInterface;
  QPixmap m_stateGateway;
  QPixmap m_stateDns;
  QPixmap m_stateInternet;
  QPixmap m_stateCaptive;
  QPixmap m_interface;
  QPixmap m_gateway;
  QPixmap m_internet;
  QPixmap m_connection;
};

#endif // ICONTHEME_H
