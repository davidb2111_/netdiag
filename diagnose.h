#ifndef DIAGNOSE_H
#define DIAGNOSE_H

#include <QObject>

#include "netdiagInterfaces.h"
#include "netdiagGateway.h"
#include "netdiagDns.h"
#include "netdiagRoutes.h"
#include "netdiagCaptivePortal.h"

class DiagnoseResult : public QObject {
  Q_OBJECT

public:
  DiagnoseResult(uint32_t mo, QString me, uint32_t st) {
    module = mo;
    message = me;
    status = st;
  }
  uint32_t module;
  QString message;
  uint32_t status;
};


class Diagnose : public QObject {
  Q_OBJECT

public:
  enum module {
    INTERFACE,
    ROUTING,
    DNS,
    CAPTIVE,
    INTERNET
  };
  Diagnose();
  void start();
  QList<DiagnoseResult*> doDiag();
  QString getModuleMessage();
  QString getErrorMessage();
  bool hasFailed();
  int getState();
  QString getInterfaceStatus();
  QString getGatewayStatus();
  QString getDnsStatus();
  QString getCaptiveStatus();
  QString getInternetStatus();

signals:
  void updated(int, QString, QString);
  void portalTrigger(bool);
  void testComplete(int, QString, int);

private:
  void setErrorState(QString, QString, int);
  void setModuleMessage(QString, QString);
  QString stepToString(int);

  QMap<QString,QString> reasons;
  QString moduleMessage;
  QString errorMessage;
  int state;
};

#endif // DIAGNOSE_H
