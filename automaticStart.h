#ifndef AUTOMATICSTART_H
#define AUTOMATICSTART_H

#include <QObject>

class AutomaticStart : public QObject {
  Q_OBJECT

public:
  static void set(bool);
};

#endif //AUTOMATICSTART_H
