#ifndef NETDIAGINTERFACES_H
#define NETDIAGINTERFACES_H

#include <QObject>

#include "netdiagGateway.h"

struct s_route {
  QString dev;
  QString scope;
  qint16 metric;
  QString proto;
  QString src;
  QString via;
  QStringList nexthops;
  qint16 weight;
  QString table;
  QString error;
};


// this class handles network interfaces

class NetdiagInterfaces : public QObject {
  Q_OBJECT
  
public:
  NetdiagInterfaces();
  void prepare();
  bool diagnose();
  QList<NetdiagGateway*> getDefaultGatewayList();
  QString getReason();

private:
  void findNetworkInterfaces();
  void findGateways();
  void parseRoutingTable();
  QMap<QString, s_route> _linuxParseRoutingTable(QStringList);

  QList<QNetworkInterface> networkInterfaces;
  QHash<QString, QList<QHostAddress>> foundNetworkInterfaces;
  QList<QHostAddress> m_addressList;
  QMap<QString, s_route> m_routesIpv4;
  QMap<QString, s_route> m_routesIpv6;
  QString m_reason;
};

#endif // NETDIAGINTERFACES_H
