#-------------------------------------------------
#
# Project created by QtCreator 2019-04-24T20:46:53
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webenginewidgets

TARGET = netindic
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        netindic.cpp \
    acMonitor.cpp \
    poller.cpp \
    pingmodel.cpp \
    netdiagInterfaces.cpp \
    netdiagGateway.cpp \
    netdiagDns.cpp \
    netdiagRoutes.cpp \
    netdiagCaptivePortal.cpp \
    netdiagInternet.cpp \
    assetsPathFinder.cpp \
    iconTheme.cpp \
    captivePortal.cpp \
    diagnoseWindow.cpp \
    preferences.cpp \
    preferencesWindow.cpp \
    automaticStart.cpp \
    sessionHandler.cpp \
    sessionManager.cpp \
    interfacesWindow.cpp \
    diagnose.cpp

HEADERS += \
        netindic.h \
    states.h \
    acMonitor.h \
    poller.h \
    pingmodel.h \
    netdiagInterfaces.h \
    netdiagGateway.h \
    netdiagDns.h \
    netdiagRoutes.h \
    netdiagCaptivePortal.h \
    netdiagInternet.h \
    iconTheme.h \
    captivePortal.h \
    config.h \
    diagnoseWindow.h \
    preferences.h \
    preferencesWindow.h \
    automaticStart.h \
    sessionHandler.h \
    sessionManager.h \
    interfacesWindow.h \
    diagnose.h

FORMS += \
        preferences.ui \
        interfaces.ui \
    diagnoseWindow.ui

TRANSLATIONS += languages/netindic_en.ts
TRANSLATIONS += languages/netindic_fr.ts

# Installs icon themes
static.path = /usr/share/netindic/static/
static.files = static/*

INSTALLS += static

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /usr/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

