#include "netdiagRoutes.h"
#include "netdiagGateway.h"
#include "preferences.h"

NetdiagRoutes::NetdiagRoutes() {

}

void NetdiagRoutes::addRoute(NetdiagGateway * gateway) {
  m_routes.append(gateway);
}

void NetdiagRoutes::addRoutes(QList<NetdiagGateway*> gateways) {
  m_routes.append(gateways);
}

bool NetdiagRoutes::diagnose() {
  bool haveIPv4Gateway = false;
  bool haveIPv6Gateway = false;
  bool canReachAllGateways = true;
  QStringList gatewayProblemReasons;
  for(NetdiagGateway *gateway : m_routes) {
    gateway->prepare(); // init test
    qDebug() << "found gateway" << gateway->address() << "reachable?" << gateway->diagnose() << "quality?" << gateway->quality() << "rtt?" << gateway->meanRtt();
    if (gateway->isIPv4())
      haveIPv4Gateway = true;
    if (gateway->isIPv6())
      haveIPv6Gateway = true;
    if (gateway->quality() <= 50 && ((gateway->isIPv4() && (Preferences::routingPolicy() == Preferences::IPV4ONLY ||
                                                           Preferences::routingPolicy() == Preferences::IPV6OPTIONAL ||
                                                           Preferences::routingPolicy() == Preferences::IPV4ANDIPV6)) ||
                                     (gateway->isIPv6() && (Preferences::routingPolicy() == Preferences::IPV6ONLY ||
                                                           Preferences::routingPolicy() == Preferences::IPV4OPTIONAL ||
                                                           Preferences::routingPolicy() == Preferences::IPV4ANDIPV6)))) {
      canReachAllGateways = false;
      gatewayProblemReasons.append(gateway->getReason());
    }
  }

  switch(Preferences::routingPolicy()) {
    case Preferences::IPV4ONLY:
      if (haveIPv6Gateway) {
        m_reason = tr("IPv6 gateway found");
        return false;
      }
      // no break here, test if ipv4 set
      [[clang::fallthrough]];
    case Preferences::IPV6OPTIONAL:
      if (!haveIPv4Gateway) {
        m_reason = tr("IPv4 gateway not found");
        return false;
      }
      break;
    case Preferences::IPV6ONLY:
      if (!haveIPv6Gateway) {
        m_reason = tr("IPv4 gateway found");
        return false;
      }
      // no break here, test if ipv6 set
      [[clang::fallthrough]];
    case Preferences::IPV4OPTIONAL:
      if (!haveIPv6Gateway) {
        m_reason = tr("IPv6 gateway not found");
        return false;
      }
      break;
    case Preferences::IPV4ANDIPV6:
      if (!haveIPv4Gateway) {
        m_reason = tr("Missing IPv4 gateway");
        return false;
      }
      if (!haveIPv6Gateway) {
        m_reason = tr("Missing IPv6 gateway");
        return false;
      }
      break;
  }
  if (!canReachAllGateways) {
    m_reason = tr("gateway not reachable:") + gatewayProblemReasons.join("\\n");
    return false;
  }
  m_reason = tr("gateway test passed");
  return true;
}

QString NetdiagRoutes::getReason() {
  return(m_reason);
}
