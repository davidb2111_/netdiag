#ifndef CAPTIVEPORTAL_H
#define CAPTIVEPORTAL_H

#include <QObject>
#include <QMainWindow>
#include <QNetworkReply>
#include <QtNetwork/QHostInfo>
#include <QtWebEngineWidgets/QWebEngineView>
#include <QUrl>

// this class test gateway reachability

class CaptivePortal : public QMainWindow {
  Q_OBJECT

public:
  explicit CaptivePortal(QWidget *parent = 0);
  ~CaptivePortal();
  void setUrl(QString);
  void open();
  void keepOpen();
  void close();
  bool isOpen();

signals:

protected:
  void closeEvent(QCloseEvent *);

private slots:

private:

  QWebEngineView *view;
  QUrl m_url;
  bool m_isOpen;
  bool m_keepOpen;
};

#endif // CAPTIVEPORTAL_H
