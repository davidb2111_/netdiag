#include <QCoreApplication>
#include <QDir>
#include <QStandardPaths>
#include <QTextStream>

#include "automaticStart.h"

void AutomaticStart::set(bool state) {
#ifdef Q_OS_LINUX
  QString autostartPath = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + QLatin1String("/autostart");
  QDir autorunDir(autostartPath);
  /* Check whether there is a directory in which to store the autorun file.
   * And then you never know ... user deleted ...
   * */
  if(!autorunDir.exists()){
      autorunDir.mkpath(autostartPath);
  }
  QFile autorunFile(autostartPath + "/" + QCoreApplication::applicationName() + ".desktop");
#endif

  if (state) {
#ifdef Q_OS_WIN32
    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    settings.setValue(APPLICATION_NAME, QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
    settings.sync();
#endif
#ifdef Q_OS_LINUX
  // Next, check the availability of the startup file
  if(!autorunFile.exists()){

      /* Next, open the file and writes the necessary data
       * with the path to the executable file, using QCoreApplication::applicationFilePath()
       * */
      if(autorunFile.open(QFile::WriteOnly)){

          QString autorunContent("[Desktop Entry]\n"
                                 "Type=Application\n"
                                 "Exec=" + QCoreApplication::applicationFilePath() + "\n"
                                 "Hidden=false\n"
                                 "NoDisplay=false\n"
                                 "X-GNOME-Autostart-enabled=true\n"
                                 "Name[en_GB]="+QCoreApplication::applicationName()+"\n"
                                 "Name="+QCoreApplication::applicationName()+"\n");
          QTextStream outStream(&autorunFile);
          outStream << autorunContent;
          // Set access rights, including on the performance of the file, otherwise the autorun does not work
          autorunFile.setPermissions(QFileDevice::ExeUser|QFileDevice::ExeOwner|QFileDevice::ExeOther|QFileDevice::ExeGroup|
                                 QFileDevice::WriteUser|QFileDevice::ReadUser);
          autorunFile.close();
      }
  }
#endif
  }else {
#ifdef Q_OS_WIN32
    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    settings.remove(APPLICATION_NAME);
#endif
#ifdef Q_OS_LINUX
  if(autorunFile.exists()) autorunFile.remove();
#endif
  }

}
