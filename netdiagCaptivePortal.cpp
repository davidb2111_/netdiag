#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTimer>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
#include "netdiagCaptivePortal.h"
#include "preferences.h"

NetdiagCaptivePortal::NetdiagCaptivePortal() {
  prepare();
}

void NetdiagCaptivePortal::prepare() {
  m_url = Preferences::captiveTestUrl();
  m_reason = "test pending";
}

void NetdiagCaptivePortal::setUrl(QString url) {
  m_url = url;
}

int NetdiagCaptivePortal::diagnose() {
  QNetworkReply * reply;
  for(uint32_t i = 0; i < Preferences::captiveMaxAttempts(); i++) {
    if ((reply = get())) {
      if (reply->error() == QNetworkReply::NoError) {
        QVariant statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
        QJsonObject docObject = doc.object();
        QJsonObject docArgsObject = docObject.value(QString("args")).toObject();
#endif
        switch(statusCode.toInt()) {
          case 200:
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
            if (doc["args"]["portal"] == "1") {
#else
            if (docArgsObject.value(QString("portal")) == "1") {
#endif
              m_reason = tr("no captive portal detected");
              return NOPORTAL;
            }else {
              m_reason = "portal test result mismatch";
              return PORTALDETECTED;
            }
            break;
          case 302:
            m_reason = "portal detected";
            return PORTALDETECTED;
            break;
        }
      }else {
        qDebug() << "get() error:" << reply->error();
      }
    }
  }
  // status can't be determined
  // either an error occured (test site is down)
  m_reason = "portal detection error";
  return PORTALDETECTIONERROR;
}

QNetworkReply * NetdiagCaptivePortal::get() {
  QEventLoop loop;
  QNetworkReply * reply;
  QNetworkAccessManager * manager = new QNetworkAccessManager(this);
  QTimer timer;
  timer.setSingleShot(true);

  connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

  reply = manager->get(QNetworkRequest(m_url));

  connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  timer.start(2000); // TODO make it configurable                                                                                                                                    
  loop.exec(); // wait for either manager() or timer() to emit signal
  if (timer.isActive()) // lookup signal received
    return reply;
  else { // 
    m_reason = tr("Can't get %1: timeout").arg(m_url);
    reply->abort();
    //QHostInfo::abortHostLookup(id);
    return NULL;
  }

}

QString NetdiagCaptivePortal::getReason() {
  return m_reason;
}

