#include <QDebug>
#include <QDir>
#include <QPixmap>

#include "assetsPathFinder.h"
#include "iconTheme.h"

IconTheme::IconTheme(QString theme) {
  QString path = assetsPathFinder({});
  if (path == nullptr)
    qDebug() << "assets not found!";

  m_theme = theme;
  m_stateOk.load(path + theme + "/checkmark_64.png");
  m_stateInterface.load(path + theme + "/error_64.png");
  m_stateGateway.load(path + theme + "/help_64.png");
  m_stateDns.load(path + theme + "/warning_64.png");
  m_stateInternet.load(path + theme + "/error_64.png");
  m_stateCaptive.load(path + theme + "/forbidden_64.png");
  m_interface.load(path + theme + "/local.png");
  m_gateway.load(path + theme + "/gateway.png");
  m_internet.load(path + theme + "/internet.png");
  m_connection.load(path + theme + "/connection.png");
} 

QPixmap IconTheme::getStatePixmap(int state) {
  switch(state) {
    case STATE_OK:
      return m_stateOk;
      break;
    case STATE_EINTERFACE:
      return m_stateInterface;
      break;
    case STATE_EGATEWAY:
      return m_stateGateway;
      break;
    case STATE_EDNS:
      return m_stateDns;
      break;
    case STATE_EINTERNET:
      return m_stateInternet;
      break;
    case STATE_ECAPTIVE:
      return m_stateCaptive;
      break;
  }
}

QPixmap IconTheme::getInterfacePixmap() {
  return m_interface;
}
QPixmap IconTheme::getGatewayPixmap() {
  return m_gateway;
}
QPixmap IconTheme::getInternetPixmap() {
  return m_internet;
}
QPixmap IconTheme::getConnectionPixmap() {
  return m_connection;
}
