<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Diagnose</name>
    <message>
        <location filename="../diagnose.cpp" line="15"/>
        <source>Interface test pending</source>
        <translation>Interface test pending</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="16"/>
        <source>Gateway test pending</source>
        <translation>Gateway test pending</translation>
    </message>
    <message>
        <source>DNS test pendig</source>
        <translation type="obsolete">DNS test pending</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="17"/>
        <source>DNS test pending</source>
        <translation>DNS test pending</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="18"/>
        <location filename="../diagnose.cpp" line="19"/>
        <source>Internet test pending</source>
        <translation>Internet test pending</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="91"/>
        <source>Full network connectivity</source>
        <translation>Full network connectivity</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="98"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="101"/>
        <source>Routing</source>
        <translation>Routing</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="104"/>
        <source>DNS</source>
        <translation>DNS</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="107"/>
        <source>Captive portal</source>
        <translation>Captive portal</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="110"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../diagnose.cpp" line="113"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>InterfacesWindow</name>
    <message>
        <location filename="../interfaces.ui" line="14"/>
        <source>Network interfaces</source>
        <translation>Network interfaces</translation>
    </message>
    <message numerus="yes">
        <location filename="../interfacesWindow.cpp" line="73"/>
        <source>IP Address(es):</source>
        <translation>
            <numerusform>IP address:</numerusform>
            <numerusform>IP addresses:</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../interfacesWindow.cpp" line="76"/>
        <source>Gateway(s):</source>
        <translation>
            <numerusform>Gateway:</numerusform>
            <numerusform>Gateways:</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../diagnoseWindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diagnoseWindow.ui" line="39"/>
        <source>Diagnose again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetdiagCaptivePortal</name>
    <message>
        <location filename="../netdiagCaptivePortal.cpp" line="43"/>
        <source>no captive portal detected</source>
        <translation>no captive portal detected</translation>
    </message>
    <message>
        <location filename="../netdiagCaptivePortal.cpp" line="83"/>
        <source>Can&apos;t get %1: timeout</source>
        <translation>Can&apos;t get %1: timeout</translation>
    </message>
</context>
<context>
    <name>NetdiagDns</name>
    <message>
        <location filename="../netdiagDns.cpp" line="31"/>
        <source>Can&apos;t resolve %1: timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagDns.cpp" line="45"/>
        <source>DNS test passed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagDns.cpp" line="49"/>
        <source>Can&apos;t resolve %1 : %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetdiagGateway</name>
    <message>
        <location filename="../netdiagGateway.cpp" line="64"/>
        <source>high latency detected on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagGateway.cpp" line="66"/>
        <source>gateway %1 partialy reachable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetdiagInterfaces</name>
    <message>
        <location filename="../netdiagInterfaces.cpp" line="16"/>
        <source>all good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagInterfaces.cpp" line="29"/>
        <source>no routed address found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetdiagInternet</name>
    <message>
        <location filename="../netdiagInternet.cpp" line="15"/>
        <source>test pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagInternet.cpp" line="25"/>
        <source>internet reachable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagInternet.cpp" line="47"/>
        <source>get reply error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagInternet.cpp" line="51"/>
        <source>get return an error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagInternet.cpp" line="56"/>
        <source>Can&apos;t get %1: timeout</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetdiagRoutes</name>
    <message>
        <location filename="../netdiagRoutes.cpp" line="43"/>
        <source>IPv6 gateway found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="50"/>
        <source>IPv4 gateway not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="56"/>
        <source>IPv4 gateway found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="63"/>
        <source>IPv6 gateway not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="69"/>
        <source>Missing IPv4 gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="73"/>
        <source>Missing IPv6 gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="79"/>
        <source>gateway not reachable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netdiagRoutes.cpp" line="82"/>
        <source>gateway test passed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Netindic</name>
    <message>
        <location filename="../netindic.cpp" line="91"/>
        <source>Netindic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="129"/>
        <source>Systray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="130"/>
        <source>Sorry, I already gave what help I could.
Maybe you should try asking a human?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="135"/>
        <source>&amp;Diagnose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="138"/>
        <source>&amp;Open captive portal window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="141"/>
        <source>&amp;Network interfaces info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="144"/>
        <source>&amp;Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netindic.cpp" line="147"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesWindow</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="57"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="105"/>
        <location filename="../preferences.ui" line="302"/>
        <location filename="../preferences.ui" line="488"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="98"/>
        <source>Check interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="115"/>
        <source>Theme:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="91"/>
        <source>Start on login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="141"/>
        <source>Network interfaces</source>
        <translation type="unfinished">Network interfaces</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="152"/>
        <location filename="../preferences.ui" line="156"/>
        <source>IPv4 only</source>
        <extracomment>Test will fail if an IPv6 router is found</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="161"/>
        <source>IPv4 mandatory and IPv6 optional</source>
        <extracomment>Must have IPv4 router and optionally IPv6 one</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="166"/>
        <source>IPv6 mandatory and IPv4 optional</source>
        <extracomment>Must have IPv6 router and optionally IPv4 one</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="171"/>
        <source>Both IPv4 and IPv6 mandatory</source>
        <extracomment>Must have IPv4 and IPv6 routers</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="176"/>
        <source>IPv6 only</source>
        <extracomment>Test will fail if an IPv4 router is found</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="184"/>
        <source>Routing Policy:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="220"/>
        <source>Routing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="228"/>
        <source>Ping count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="267"/>
        <source>DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="288"/>
        <source>Hostname to query: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="295"/>
        <source>Query timeout: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="344"/>
        <source>Captive portal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <location filename="../preferences.ui" line="442"/>
        <source>Test URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="359"/>
        <location filename="../preferences.ui" line="435"/>
        <source>Test max attempts:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="388"/>
        <location filename="../preferences.ui" line="458"/>
        <source>This URL should return a JSON object with portal attributes = 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="427"/>
        <source>Internet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="481"/>
        <source>Querry Timeout:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="29"/>
        <source>Systray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="30"/>
        <source>I couldn&apos;t detect any system tray on this system.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>diagnose</name>
    <message>
        <location filename="../diagnoseWindow.cpp" line="29"/>
        <source>Diagnose</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>preferences</name>
    <message>
        <location filename="../preferencesWindow.cpp" line="22"/>
        <source>Netindic preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
