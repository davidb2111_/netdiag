
#include <QDebug>
#include <QDir>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include "acMonitor.h"

AcMonitor::AcMonitor() {
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(refreshStatus()));
  timer->setInterval(1000);
  timer->start();
  m_online = isOnline();
}

// read linux power class items, return true if AC is online
bool AcMonitor::isOnline() {
  QDir power_dir(SYSFS_POWER_DIR);
  foreach(QString dir, power_dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
    if (power_dir.exists(dir + "/online")) {
      if (readIntSysFile(power_dir.filePath(dir + "/online").toLatin1().constData()) == 1) {
        return true;
      }
    }
  }
  return false;
}

// poller, refresh AC status and emits signals when AC status has changed
void AcMonitor::refreshStatus() {
  bool newOnline = isOnline();
  if (newOnline != m_online) {
    qDebug() << "AC status changed:" << newOnline;
    if (newOnline)
      emit onAC();
    else
      emit onBattery();
    emit powerChanged(newOnline);
    m_online = newOnline;
  }
}

// read a text file and return an integer
int AcMonitor::readIntSysFile(const char * fileName) {
  int data = 0;
  QFile f(fileName);
  if (f.open(QIODevice::ReadOnly)) {
    QTextStream stream(&f);
    stream >> data;
  }
  return data;
}
