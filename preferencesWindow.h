#ifndef PREFERENCESWINDOW_H
#define PREFERENCESWINDOW_H

#include <QAbstractButton>
#include <QLabel>
#include <QMainWindow>

#include "preferences.h"

namespace Ui {
class PreferencesWindow;
}

class PreferencesWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit PreferencesWindow(QWidget *parent = 0);
  ~PreferencesWindow();
  void open();

private slots:
  void saveClicked();
  void applyClicked();
  void buttonDispatcher(QAbstractButton *);

private:
  QStringList getThemeList();
  void updatePreferences();

  Ui::PreferencesWindow *ui;

signals:
  void preferencesChanged();
};

#endif // PREFERENCESWINDOW_H
