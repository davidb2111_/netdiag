#include <QtNetwork/QHostInfo>
#include <QTimer>
#include <QEventLoop>
#include "netdiagDns.h"
#include "preferences.h"

NetdiagDns::NetdiagDns() {
  prepare();
}

void NetdiagDns::prepare() {
  m_reason = "";
}

void NetdiagDns::setHostname(QString name) {
  m_hostname = name;
}
// FIXME QHostInfo keeps a cache
//       cache should be cleared or lookup must be forced
bool NetdiagDns::diagnose() {
  QEventLoop loop;
  int id = QHostInfo::lookupHost(m_hostname, this, SLOT(lookedUp(QHostInfo)));
  QTimer timer;
  timer.setSingleShot(true);
  connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
  connect(this, SIGNAL(resolved()), &loop, SLOT(quit()));

  timer.start(Preferences::dnsTimeout() * 1000);
  loop.exec(); // wait for either lookupHost() or timer() to emit signal
  if (!timer.isActive()) { // timeout() signal received
    m_reason = tr("Can't resolve %1: timeout").arg(m_hostname);
    QHostInfo::abortHostLookup(id);
    m_result = false;
  }
  return m_result;
}

QString NetdiagDns::getReason() {
  return m_reason;
}

void NetdiagDns::lookedUp(const QHostInfo &host) {
  switch(host.error()) {
    case QHostInfo::NoError:
      m_reason = tr("DNS test passed");
      m_result = true;
      break;
    default:
      m_reason = tr("Can't resolve %1 : %2").arg(m_hostname).arg(host.errorString());
      m_result = false;
      break;
  }
  emit resolved();
}
