#include <QDesktopWidget>
#include <QHBoxLayout>
#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTimer>
#include <QEventLoop>
#include <QJsonDocument>
//#include <QtWebView/QtWebView>
#include <QtWebEngineWidgets/QWebEngineView>

#include "captivePortal.h"

CaptivePortal::CaptivePortal(QWidget * parent) :
  QMainWindow(parent) {

  resize(800, 600); // size is fixed for now...
  m_isOpen = false;
  m_keepOpen = false;

  view = new QWebEngineView(this);
  setCentralWidget(view);

  int width = frameGeometry().width();
  int height = frameGeometry().height();
  QDesktopWidget wid;
  int screenWidth = wid.screen()->width();
  int screenHeight = wid.screen()->height();
  setGeometry((screenWidth/2)-(width/2),(screenHeight/2)-(height/2),width,height);

}

CaptivePortal::~CaptivePortal() {
}

void CaptivePortal::setUrl(QString url) {
  m_url.setUrl(url);
}

void CaptivePortal::keepOpen() {
  m_keepOpen = true;
  open();
}

void CaptivePortal::open() {
  view->load(m_url);
  show();
  m_isOpen = true;
}
void CaptivePortal::close() {
  if (!m_keepOpen) {
    hide();
    m_isOpen = false;
  }
}

void CaptivePortal::closeEvent(QCloseEvent *event) {
  m_isOpen = false;
  m_keepOpen = false;
  qDebug() << "portal window closed";
  QWidget::closeEvent(event);
}

bool CaptivePortal::isOpen() {
  return m_isOpen;
}
