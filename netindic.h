#ifndef WINDOW_H
#define WINDOW_H

#include <QSystemTrayIcon>
#include <QThread>

#ifndef QT_NO_SYSTEMTRAYICON

#include <QDialog>

#include "poller.h"
#include "iconTheme.h"
#include "captivePortal.h"
#include "diagnoseWindow.h"
#include "preferences.h"
#include "preferencesWindow.h"
#include "interfacesWindow.h"
#include "sessionManager.h"
#include "acMonitor.h"

class QAction;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QSpinBox;
class QTextEdit;

class Netindic : public QWidget {
  Q_OBJECT
  QThread workerThread;

public:
  Netindic();
  ~Netindic();

public slots:
  void openDiagnoseWindow();
  void openPreferencesWindow();
  void openCaptivePortalWindow();
  void openInterfacesWindow();
  void handleCaptivePortalWindow(bool);

protected:
//  void closeEvent(QCloseEvent *event) override;

private slots:
  void setState(int, QString, QString);
  void iconActivated(QSystemTrayIcon::ActivationReason reason);
  void messageClicked();
  void preferencesChanged();
  void setPollerInterval();

private:
  void showMessage(QString, QString);
  void setIcon(int index);
  void createIconGroupBox();
  void createMessageGroupBox();
  void createActions();
  void createTrayIcon();
  void quit();

  int oldState;

  Poller * poller;

  QGroupBox *iconGroupBox;
  QLabel *iconLabel;
  QComboBox *iconComboBox;
  QCheckBox *showIconCheckBox;

  QGroupBox *messageGroupBox;
  QLabel *typeLabel;
  QLabel *durationLabel;
  QLabel *durationWarningLabel;
  QLabel *titleLabel;
  QLabel *bodyLabel;
  QComboBox *typeComboBox;
  QSpinBox *durationSpinBox;
  QLineEdit *titleEdit;
  QTextEdit *bodyEdit;
  QPushButton *showMessageButton;

  QAction *diagnoseAction;
  QAction *openCaptiveWindowAction;
  QAction *interfacesAction;
  QAction *preferencesAction;
  QAction *portalAction;
  QAction *quitAction;

  QSystemTrayIcon *trayIcon;
  QMenu *trayIconMenu;
  IconTheme * iconTheme;
  SessionManager * sessionManager;
  AcMonitor * acMonitor;

  // Windows
  DiagnoseWindow diagnoseWindow;
  PreferencesWindow * preferencesWindow;
  InterfacesWindow * interfacesWindow;
  CaptivePortal captivePortalWindow;
};

#endif // QT_NO_SYSTEMTRAYICON

#endif
