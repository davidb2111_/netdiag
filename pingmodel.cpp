#include <QString>
#include <QDebug>
#include <QRegularExpression>
#include <QProcess>
#include <QObject>
#include <QtNetwork/QHostAddress>
#include <QProcessEnvironment>

#include "pingmodel.h"

PingModel::PingModel(QObject *parent) :
    QObject(parent) {
    ping = new QProcess(this);
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("LANG","C");
    ping->setProcessEnvironment(env);
    connect(ping, SIGNAL(readyRead()), this, SLOT(readResult()));
    connect(ping, SIGNAL(finished(int)), this, SLOT(readResult()));
    success = false;
}

PingModel::~PingModel(){
}

void PingModel::setHost(QHostAddress address) {
  host = address;
}

void PingModel::setHost(QHostAddress address, QString dev) {
  host = QHostAddress(QString(address.toString() + "%" + dev));
}

float PingModel::getRtt() {
  return rtt;
}
bool PingModel::isSuccess() {
  return success;
}

void PingModel::readResult(){
  QRegularExpression re("time=([^\\s]+)\\s+(.*)");
  QList<QByteArray> readLines = ping->readLine().split('\n');
  for (QByteArray r : readLines) {
    QRegularExpressionMatch match = re.match(r);
    if (match.hasMatch()) {
      if (match.captured(2) == "ms") {
        rtt = match.captured(1).toFloat()/1000;
      }else
        rtt = match.captured(1).toFloat();
      success = true;
    }
  }
}

void PingModel::run(){
  if(ping and !host.isNull()) {
    QString command = "ping";
    QStringList args;
    args << "-n" << "-c" << "1" << "-w" <<  "1" << host.toString();
    ping->start(command, args);
    ping->waitForStarted(7000);
    ping->waitForFinished(5000);
  }
}

bool PingModel::finished(){
    return ping->atEnd();
}
