#include <QtGlobal>
#include <QProcess>
#include <QRegularExpression>
#include <QtNetwork/QNetworkInterface>

#include "netdiagInterfaces.h"
#include "netdiagGateway.h"
#include "config.h"

NetdiagInterfaces::NetdiagInterfaces() {

}

void NetdiagInterfaces::prepare() {
  // empty last error message
  m_reason = tr("all good");
  // get all network interfaces
  networkInterfaces = QNetworkInterface::allInterfaces();
  // clear IP address list
  m_addressList.clear();
  // ip route table can be executed at this stage
  parseRoutingTable();
}

bool NetdiagInterfaces::diagnose() {
  findNetworkInterfaces();
  if (m_addressList.size() && !getDefaultGatewayList().isEmpty())
    return true;
  m_reason = tr("no routed address found");
  return false;
}

QString NetdiagInterfaces::getReason() {
  return m_reason;
}

// provision a hash of interfaces and its list of addressses
// and a list of routable addresses
void NetdiagInterfaces::findNetworkInterfaces() {
  if (!networkInterfaces.isEmpty()) {
    for(int i=0; i < networkInterfaces.size(); i++) {
      QList<QHostAddress> foundAddresses;
      unsigned int flags = networkInterfaces[i].flags();
      bool isLoopback = (bool)(flags & QNetworkInterface::IsLoopBack);
      bool isP2P = (bool)(flags & QNetworkInterface::IsPointToPoint);
      bool isRunning = (bool)(flags & QNetworkInterface::IsRunning);

      // If this interface isn't running, we don't care about it
      if ( !isRunning ) continue;
      // We only want valid interfaces that aren't loopback/virtual and not point to point
      if ( !networkInterfaces[i].isValid() || isLoopback || isP2P ) continue;
      QList<QHostAddress> addresses = networkInterfaces[i].allAddresses();
      for(int a=0; a < addresses.size(); a++) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 11, 0)
        if ( !addresses[a].isGlobal() ) continue;
#else
        if ( !addresses[a].isInSubnet(QHostAddress("2000::"), 3) ) continue;
#endif

        // Ignore non-ipv4 or ipv6 addresses
        if (QAbstractSocket::IPv4Protocol != addresses[a].protocol() and
            QAbstractSocket::IPv6Protocol != addresses[a].protocol()){
          qDebug() << addresses[a].toString() << "Unknown or invalid address";
          continue;
        }

        QString ip = addresses[a].toString();
        if ( ip.isEmpty() ) continue;
        foundAddresses.append(addresses[a]);
        m_addressList.append(addresses[a]);
      }
      foundNetworkInterfaces.insert(networkInterfaces[i].humanReadableName(), foundAddresses);
    }
  }
}

void NetdiagInterfaces::parseRoutingTable() {
#ifdef Q_OS_LINUX
  QProcess process;
  //process.start("ip -4 route show table main");
  process.start(CMD_IPV4_ROUTE);
  process.waitForFinished(-1); // will wait forever until finished
  QString stdout = process.readAllStandardOutput();
  QStringList ipRoute = stdout.split("\n");
  m_routesIpv4.clear();
  m_routesIpv4 = _linuxParseRoutingTable(ipRoute);

  //process.start("ip -6 route show table main");
  process.start(CMD_IPV6_ROUTE);
  process.waitForFinished(-1); // will wait forever until finished
  stdout = process.readAllStandardOutput();
  ipRoute = stdout.split("\n");
  m_routesIpv6.clear();
  m_routesIpv6 = _linuxParseRoutingTable(ipRoute);
#else
  qDebug() << "route parsing not supported for your platform";
#endif
}

QList<NetdiagGateway*> NetdiagInterfaces::getDefaultGatewayList() {
  QList<NetdiagGateway*> gateways;
  for (QMap<QString, s_route>::const_iterator it = m_routesIpv4.cbegin(), end = m_routesIpv4.cend(); it != end; ++it) {
    if (it.key() == "default") {
      if (it.value().via.length()) 
        gateways << new NetdiagGateway(QHostAddress(it.value().via), it.value().dev);
      if (it.value().nexthops.size()) 
        foreach(QString g, it.value().nexthops)
          gateways << new NetdiagGateway(QHostAddress(g), it.value().dev);
    }
  }
  for (QMap<QString, s_route>::const_iterator it = m_routesIpv6.cbegin(), end = m_routesIpv6.cend(); it != end; ++it) {
    if (it.key() == "default" or it.key() == "2000::/3") {
      if (it.value().via.length()) 
        gateways << new NetdiagGateway(QHostAddress(it.value().via), it.value().dev);
      if (it.value().nexthops.size()) 
        foreach(QString g, it.value().nexthops)
          gateways << new NetdiagGateway(QHostAddress(g), it.value().dev);
    }
  }
  return(gateways);
}

QDebug operator<<(QDebug debug, const s_route &s)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "dev:" << s.dev << ", via:" << s.via << ", nexthops:" << s.nexthops;

    return debug;
}

// low level parsing routine
// can parse both IPv4 and IPv6 routes 
// support dev, proto, via, scope, src metric and nexthop via attributes
QMap<QString, s_route> NetdiagInterfaces::_linuxParseRoutingTable(QStringList routes) {
  QMap<QString, s_route> parsedRoutes;
  QMap<QString, s_route>::iterator last_route;
  QRegularExpression re("(dev|proto|nexthop via|via|scope|src|metric)\\s+([^\\s]+)");
  QList<QHostAddress> nexthops;
  for(int i = 0; i < routes.size(); i++) {
    s_route route;
    QString prefix = routes[i].split(" ")[0];
    QRegularExpressionMatchIterator j = re.globalMatch(routes[i]);
    bool is_nexthop = false;
    while(j.hasNext()) {
      QRegularExpressionMatch match = j.next();
      if (match.captured(1) == "nexthop via") {
        // nexthops (multi route) are multi-lined
        // add it to latest route found
        last_route->nexthops << match.captured(2);
        is_nexthop = true;
      }else if (match.captured(1) == "via") {
        route.via = match.captured(2);
      }else if (match.captured(1) == "dev") {
        route.dev = match.captured(2);
      }else if (match.captured(1) == "scope") {
        route.scope = match.captured(2);
      }else if (match.captured(1) == "metric") {
        route.metric = match.captured(2).toInt();
      }
    }
    if (!is_nexthop) {
      last_route = parsedRoutes.insertMulti(prefix, route);
    }
  }
  return(parsedRoutes);
}
