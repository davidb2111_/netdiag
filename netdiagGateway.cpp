#include <QProcess>
#include <QRegularExpression>

#include "netdiagGateway.h"
#include "config.h"
#include "pingmodel.h"
#include "preferences.h"

//
// this class should be used like this:
// create a new instance with gateway IP address
// then
// prepare() // init tests
// diagnose() // run tests
// quality() // compute quality for diagnose results
//

NetdiagGateway::NetdiagGateway(QHostAddress address, QString dev) {
  m_gatewayAddress = address;
  m_dev = dev;
  prepare();
}

void NetdiagGateway::prepare() {
  m_pingCount = 0;
  m_pingTotal = Preferences::routePingCount();
  m_success = false;
  m_meanRtt = 0;
}

bool NetdiagGateway::diagnose() {
  if (!ping()) {
    m_reason = "can't reach " + m_gatewayAddress.toString();
    return false;
  }
  return true;
}

QString NetdiagGateway::getReason() {
  return m_reason;
}

// do X ping a host
// compute mean rtt
// return true if at least one ping is successful
bool NetdiagGateway::ping() {
  for(int i=0; i < m_pingTotal; i++) {
    PingModel ping;
    if (isIPv4())
      ping.setHost(m_gatewayAddress);
    if (isIPv6())
      ping.setHost(m_gatewayAddress, m_dev);
    ping.run();
    if (ping.isSuccess()) {
      m_meanRtt += ping.getRtt();
      m_pingCount++;
      if (m_pingCount > 1)
        m_meanRtt /= 2;
      m_success = true;
    }
  }
  // check latency
  if (m_meanRtt >= 0.1)
    m_reason = tr("high latency detected on %1").arg(address());
  if (m_pingCount < m_pingTotal)
    m_reason = tr("gateway %1 partialy reachable").arg(address());
  return m_success;
}

// return a global score of gateway reachness
// quality score is calculted on
// number of ping responses
// mean rtt
int NetdiagGateway::quality() {
  int quality;
  if (!m_success)
    return(0);

  quality = ((float)m_pingCount/(float)m_pingTotal)*100.0;
  if (m_meanRtt < 0.05)
    return quality;
  if (m_meanRtt < 0.1)
    return quality/1.2;
  if (m_meanRtt < 1)
    return quality/1.7;
  return quality/2.0;
}


float NetdiagGateway::meanRtt() {
  return m_meanRtt;
}

QString NetdiagGateway::address() {
  return(m_gatewayAddress.toString());
}

bool NetdiagGateway::isIPv4() {
  if (m_gatewayAddress.protocol() == QAbstractSocket::IPv4Protocol)
    return(true);
  return(false);
}

bool NetdiagGateway::isIPv6() {
  if (m_gatewayAddress.protocol() == QAbstractSocket::IPv6Protocol)
    return(true);
  return(false);
}

QString NetdiagGateway::device() {
  return m_dev;
}
