#ifndef NETDIAGROUTES_H
#define NETDIAGROUTES_H

#include <QObject>
#include "netdiagGateway.h"

class NetdiagRoutes : public QObject {
  Q_OBJECT
  
public:
  NetdiagRoutes();
  void addRoute(NetdiagGateway*);
  void addRoutes(QList<NetdiagGateway*>);
  bool diagnose();

  QString getReason();

private:
  QList<NetdiagGateway*> m_routes; 
  QString m_reason;

};

#endif // NETDIAGROUTES_H

