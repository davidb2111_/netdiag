#include <QApplication>

#ifndef QT_NO_SYSTEMTRAYICON

#include <QMessageBox>
#include <QSettings>
#include <QTimer>
#include <QTranslator>

#include "netindic.h"

int main(int argc, char *argv[]) {
//  Q_INIT_RESOURCE(netindic);

  QApplication app(argc, argv);


  app.setOrganizationName("netindic");
  app.setOrganizationDomain("NetDiag");
  app.setApplicationName("netindic");

  QSettings::setDefaultFormat(QSettings::IniFormat);

  qDebug() << "Language:" << QLocale::system().name();
  QTranslator translator;
  //if (translator.load(QLocale(QString("fr")), QLatin1String("netindic"), QLatin1String("_"), QLatin1String("languages")))
  if (translator.load(QLocale(QLocale::system().name()), QLatin1String("netindic"), QLatin1String("_"), QLatin1String(":/languages")))
        app.installTranslator(&translator);

  if (!QSystemTrayIcon::isSystemTrayAvailable()) {
      QMessageBox::critical(0, QObject::tr("Systray"),
                               QObject::tr("I couldn't detect any system tray "
                                           "on this system."));
      return 1;
  }
  QApplication::setQuitOnLastWindowClosed(false);

  Netindic netindic;
//  netindic.show();
  return app.exec();
}

#else

#include <QLabel>
#include <QDebug>

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  QString text("QSystemTrayIcon is not supported on this platform");

  QLabel *label = new QLabel(text);
  label->setWordWrap(true);

  label->show();
  qDebug() << text;

  app.exec();
}

#endif
