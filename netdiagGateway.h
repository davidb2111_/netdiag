#ifndef NETDIAGGATEWAY_H
#define NETDIAGGATEWAY_H

#include <QObject>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QNetworkConfigurationManager>

// this class test gateway reachability

class NetdiagGateway : public QObject {
  Q_OBJECT

public:
  NetdiagGateway(QHostAddress, QString);
  void prepare();
  bool diagnose();
  void populateGateways(QStringList);
  QString address();
  bool isIPv4();
  bool isIPv6();
  int quality();
  float meanRtt();
  QString device();

  QStringList getDefaultGatewayList();
  QString getReason();

private:
  bool ping();
  QHostAddress m_gatewayAddress;
  QString m_dev; // aka interface
  bool m_success;
  int m_pingCount;
  int m_pingTotal;
  float m_meanRtt;
  QString m_reason;
};

#endif // NETDIAGGATEWAY_H
