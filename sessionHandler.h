#ifndef SESSIONHANDLER_H
#define SESSIONHANDLER_H

#include <QNetworkConfiguration>
#include <QNetworkSession>
#include <QObject>

class SessionHandler : public QNetworkSession {
  Q_OBJECT

public:
  SessionHandler(QNetworkConfiguration);
  void cleanup();

signals:
  void networkConfigurationStateChanged(QString);

private slots:
  void emitStateChanged(QNetworkSession::State);

};

#endif // SESSIONHANDLER_H
