#include <QNetworkConfiguration>

#include "sessionHandler.h"

SessionHandler::SessionHandler(QNetworkConfiguration connectionConfig)
  :QNetworkSession(connectionConfig) {
  connect(this, SIGNAL(stateChanged(QNetworkSession::State)), this, SLOT(emitStateChanged(QNetworkSession::State)));
  qDebug() << "Start monitoring" << connectionConfig.name()
           << "with current state:" << state();
}

void SessionHandler::cleanup() {
  qDebug() << configuration().name() << "cleanup";
  disconnect(this, SIGNAL(stateChanged(QNetworkSession::State)), this, SLOT(emitStateChanged(QNetworkSession::State)));
}

void SessionHandler::emitStateChanged(QNetworkSession::State newState) {
  qDebug() << configuration().name() << "state changed" << newState;
  emit networkConfigurationStateChanged(configuration().name()) ;
}

