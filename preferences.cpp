
#include <QSettings>

#include "preferences.h"

Preferences::Preferences() {
}

//void Preferences::loadSettings() {
//  QSettings settings("netindic", "netindic");
//  m_theme = settings.value("theme", QString("iconic")).toString();
//  m_checkInterval = settings.value("check_interval", 20).toInt();
//  m_testUrl = settings.value("test_url", QString("http://httpbin.org")).toString();
//  m_routingPolicy = settings.value("routing_policy", IPV4ANDIPV6).toInt();
//  testUrlCount = settings.value("test_url_count", 5).toInt();
//  portalTestUrl = settings.value("portal_test_url", QString("http://httpbin.org/get?portal=1")).toString();
//  testDns = settings.value("test_dns", QString("root-servers.org")).toString();
//  testDnsTimeout = settings.value("test_dns_timeout", 2).toInt();
//  gatewayPingCount = settings.value("gateway_ping_count", 5).toInt();
//}

QString Preferences::theme() {
  QSettings settings;
  return settings.value("theme", QString("iconic")).toString();
}
void Preferences::setTheme(QString newTheme) {
  QSettings settings;
  settings.setValue("theme", newTheme);
  settings.sync();
}

uint32_t Preferences::checkIntervalAC() {
  QSettings settings;
  return settings.value("check_interval", 20).toInt();
}

void Preferences::setCheckIntervalAC(uint32_t newCheckInterval) {
  QSettings settings;
  settings.setValue("check_interval", newCheckInterval);
  settings.sync();
}

uint32_t Preferences::checkIntervalBattery() {
  QSettings settings;
  return settings.value("check_interval_battery", 60).toInt();
}

void Preferences::setCheckIntervalBattery(uint32_t newCheckInterval) {
  QSettings settings;
  settings.setValue("check_interval_battery", newCheckInterval);
  settings.sync();
}

QString Preferences::testUrl() {
  QSettings settings;
  return settings.value("test_url", QString("http://httpbin.org")).toString();
}
void Preferences::setTestUrl(QString newTestUrl) {
  QSettings settings;
  settings.setValue("test_url", newTestUrl);
  settings.sync();
}

uint32_t Preferences::routingPolicy() {
  QSettings settings;
  return settings.value("routing_policy", IPV4ANDIPV6).toInt();
}

void Preferences::setRoutingPolicy(uint32_t newRoutingPolicy) {
  QSettings settings;
  settings.setValue("routing_policy", newRoutingPolicy);
  settings.sync();
}

uint32_t Preferences::routePingCount() {
  QSettings settings;
  return settings.value("routing_ping_count", 5).toInt();
}

void Preferences::setRoutePingCount(uint32_t newRoutePingCount) {
  QSettings settings;
  settings.setValue("routing_ping_count", newRoutePingCount);
  settings.sync();
}

bool Preferences::automaticStart() {
  QSettings settings;
  return settings.value("automatic_start", false).toBool();
}
void Preferences::setAutomaticStart(bool newAutomaticStart) {
  QSettings settings;
  settings.setValue("automatic_start", newAutomaticStart);
  settings.sync();
}

uint32_t Preferences::dnsTimeout() {
  QSettings settings;
  return settings.value("dns_timeout", 5).toInt();
}
void Preferences::setDnsTimeout(uint32_t newDnsTimeout) {
  QSettings settings;
  settings.setValue("dns_timeout", newDnsTimeout);
  settings.sync();
}

QString Preferences::dnsHostname() {
  QSettings settings;
  return settings.value("dns_hostname", QString("root-servers.org")).toString();
}
void Preferences::setDnsHostname(QString newDnsHostname) {
  QSettings settings;
  settings.setValue("dns_hostname", newDnsHostname);
  settings.sync();
}

QString Preferences::captiveTestUrl() {
  QSettings settings;
  return settings.value("captive_test_url", QString("http://httpbin.org/get?portal=1")).toString();
}
void Preferences::setCaptiveTestUrl(QString newUrl) {
  QSettings settings;
  settings.setValue("captive_test_url", newUrl);
  settings.sync();
}

uint32_t Preferences::captiveMaxAttempts() {
  QSettings settings;
  return settings.value("captive_max_attempts", 5).toInt();
}
void Preferences::setCaptiveMaxAttempts(uint32_t newMaxAttempts) {
  QSettings settings;
  settings.setValue("captive_max_attempts", newMaxAttempts);
  settings.sync();
}

QString Preferences::internetTestUrl() {
  QSettings settings;
  return settings.value("internet_test_url", QString("http://httpbin.org/")).toString();
}
void Preferences::setInternetTestUrl(QString newUrl) {
  QSettings settings;
  settings.setValue("internet_test_url", newUrl);
  settings.sync();
}

uint32_t Preferences::internetMaxAttempts() {
  QSettings settings;
  return settings.value("internet_max_attempts", 5).toInt();
}
void Preferences::setInternetMaxAttempts(uint32_t newMaxAttempts) {
  QSettings settings;
  settings.setValue("internet_max_attempts", newMaxAttempts);
  settings.sync();
}

uint32_t Preferences::internetTimeout() {
  QSettings settings;
  return settings.value("internet_timeout", 2).toInt();
}
void Preferences::setInternetTimeout(uint32_t newTimeout) {
  QSettings settings;
  settings.setValue("internet_timeout", newTimeout);
  settings.sync();
}
