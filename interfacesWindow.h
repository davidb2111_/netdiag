#ifndef INTERFACESWINDOW_H
#define INTERFACESWINDOW_H

#include <QAbstractButton>
#include <QLabel>
#include <QNetworkInterface>
#include <QToolBox>
#include <QWidget>


namespace Ui {
class InterfacesWindow;
}

class InterfacesWindow : public QWidget {
  Q_OBJECT

public:
  explicit InterfacesWindow(QWidget *parent = 0);
  ~InterfacesWindow();
  void open();

protected:
  void closeEvent(QCloseEvent *);

private slots:

private:

  QWidget * forgeInterfacePage(QNetworkInterface);
  QStringList findGatewayForInterface(QString);

  Ui::InterfacesWindow *ui;
  QToolBox * gen_toolbox;
};

#endif // INTERFACESWINDOW_H
