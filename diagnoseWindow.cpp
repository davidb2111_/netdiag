#include <QApplication>
#include <QDebug>
#include <QDesktopWidget>
#include <QDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QStyle>
#include <QTimer>
#include <QVBoxLayout>
#include "diagnose.h"
#include "diagnoseWindow.h"
#include "iconTheme.h"
#include "preferences.h"

inline void CenterWidgets(QWidget *widget, QWidget *host );

DiagnoseWindow::DiagnoseWindow(QWidget *parent) :
    QDialog(parent)
{

  resize(515,215);
  int width = frameGeometry().width();
  int height = frameGeometry().height();
  QDesktopWidget wid;
  int screenWidth = wid.screen()->width();
  int screenHeight = wid.screen()->height();
  setGeometry((screenWidth/2)-(width/2),(screenHeight/2)-(height/2),width,height);

  setWindowTitle(QApplication::translate("diagnose", "Diagnose"));
  QHBoxLayout * h_layout = new QHBoxLayout;
  QVBoxLayout * vb1 = new QVBoxLayout;
  QVBoxLayout * vb2 = new QVBoxLayout;
  QVBoxLayout * vb3 = new QVBoxLayout;
  this->setLayout(h_layout);

  IconTheme * iconTheme = new IconTheme(Preferences::theme());
  QLabel * l_interface = new QLabel(this);
  QLabel * l_lan = new QLabel(this);
  QLabel * l_gateway = new QLabel(this);
  QLabel * l_wan = new QLabel(this);
  QLabel * l_internet = new QLabel(this);
  l_interfaceText = new QLabel(this);
  l_interfaceText->setAlignment(Qt::AlignHCenter);
  l_gatewayText = new QLabel(this);
  l_gatewayText->setAlignment(Qt::AlignHCenter);
  l_dnsText = new QLabel(this);
  l_dnsText->setAlignment(Qt::AlignHCenter);
  l_captiveText = new QLabel(this);
  l_captiveText->setAlignment(Qt::AlignHCenter);
  l_internetText = new QLabel(this);
  l_internetText->setAlignment(Qt::AlignHCenter);

  l_interface->setPixmap(iconTheme->getInterfacePixmap().scaled(QSize(128,128), Qt::KeepAspectRatio));
  l_interface->setAlignment(Qt::AlignHCenter);
  l_lan->setPixmap(iconTheme->getConnectionPixmap());
  l_gateway->setPixmap(iconTheme->getGatewayPixmap().scaled(QSize(128,128), Qt::KeepAspectRatio));
  l_gateway->setAlignment(Qt::AlignHCenter);
  l_wan->setPixmap(iconTheme->getConnectionPixmap());
  l_internet->setPixmap(iconTheme->getInternetPixmap().scaled(QSize(128,128), Qt::KeepAspectRatio));
  l_internet->setAlignment(Qt::AlignHCenter);
  vb1->addWidget(l_interface);
  vb1->addWidget(l_interfaceText);
  vb2->addWidget(l_gateway);
  vb2->addWidget(l_gatewayText);
  vb3->addWidget(l_internet);
  vb3->addWidget(l_dnsText);
  vb3->addWidget(l_captiveText);
  vb3->addWidget(l_internetText);
  h_layout->addLayout(vb1);
  h_layout->addWidget(l_lan);
  h_layout->addLayout(vb2);
  h_layout->addWidget(l_wan);
  h_layout->addLayout(vb3);
}

void DiagnoseWindow::open() {
  l_interfaceText->setText("");
  l_gatewayText->setText("");
  l_dnsText->setText("");
  l_captiveText->setText("");
  l_internetText->setText("");
  diagnose = new Diagnose();
  connect(diagnose, SIGNAL(testComplete(int, QString, int)), this, SLOT(refreshWindow(int, QString, int)));
  qDebug() << "DiagnoseWindow::open()";
  CenterWidgets(&window, NULL);
  show();
  diagnose->moveToThread(&workerThread);
  workerThread.start();
  diagnose->doDiag();
}

void DiagnoseWindow::close() {
  qDebug() << "DiagnoseWindow::close";
  workerThread.quit();
  workerThread.wait();
}

void DiagnoseWindow::closeEvent(QCloseEvent * event) {
  close();
  QWidget::closeEvent(event);
}

void DiagnoseWindow::refreshWindow(int module, QString message, int status) {
  qDebug() << "update diagnose window";
  switch (module) {
    case Diagnose::INTERFACE:
      l_interfaceText->setText(message);
      break;
    case Diagnose::ROUTING:
      l_gatewayText->setText(message);
      break;
    case Diagnose::DNS:
      l_dnsText->setText(message);
      break;
    case Diagnose::CAPTIVE:
      l_captiveText->setText(message);
      break;
    case Diagnose::INTERNET:
      l_internetText->setText(message);
      break;
  }
}

DiagnoseWindow::~DiagnoseWindow() {
  qDebug() << "DiagnoseWindow::~DiagnoseWindow()";
}

// short helper for centering a widget relativaly from another
inline void CenterWidgets(QWidget *widget, QWidget *host = 0) {
    if (!host)
        host = widget->parentWidget();

    if (host) {
        auto hostRect = host->geometry();
        widget->move(hostRect.center() - widget->rect().center());
    }
    else {
        QRect screenGeometry = QApplication::desktop()->screenGeometry();
        int x = (screenGeometry.width() - widget->width()) / 2;
        int y = (screenGeometry.height() - widget->height()) / 2;
        widget->move(x, y);
    }
}
