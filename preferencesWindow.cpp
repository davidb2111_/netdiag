#include <QApplication>
#include <QStyle>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDesktopWidget>
#include <QLabel>
#include <QDebug>
#include <QMainWindow>
#include <QDir>
#include <QDirIterator>
#include "assetsPathFinder.h"
#include "automaticStart.h"
#include "preferences.h"
#include "preferencesWindow.h"
#include "ui_preferences.h"

PreferencesWindow::PreferencesWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::PreferencesWindow)
{
  ui->setupUi(this);
  setWindowTitle(QApplication::translate("preferences", "Netindic preferences"));
}

void PreferencesWindow::open() {
  QStringList themes;
  int i = 0;
  qDebug() << "PreferencesWindow::open()";

  // populate theme list
  themes = getThemeList();
  ui->gen_theme->clear();
  for(QString theme : themes) {
    ui->gen_theme->addItem(theme);
    if (theme == Preferences::theme())
      ui->gen_theme->setCurrentIndex(i);
    i++;
  }

  // set routing policy combo box
  ui->routing_policy->setCurrentIndex(Preferences::routingPolicy());

  // set check interval
  ui->gen_check_interval_ac->setValue(Preferences::checkIntervalAC());
  ui->gen_check_interval_battery->setValue(Preferences::checkIntervalBattery());

  // set route ping count
  ui->route_ping_count->setValue(Preferences::routePingCount());

  // set automatic start checkbox
  ui->gen_automatic_start->setChecked(Preferences::automaticStart());

  // set dns hostname
  ui->gen_dns_hostname->setText(Preferences::dnsHostname());

  // set dns timeout
  ui->gen_dns_timeout->setValue(Preferences::dnsTimeout());

  // set captive test url
  ui->gen_captive_test_url->setText(Preferences::captiveTestUrl());

  // set captive test max attempts
  ui->gen_captive_test_max_attempts->setValue(Preferences::captiveMaxAttempts());

  // set internet test url
  ui->gen_internet_test_url->setText(Preferences::internetTestUrl());

  // set internet test max attempts
  ui->gen_internet_test_max_attempts->setValue(Preferences::internetMaxAttempts());

  // set internet querry timeout
  ui->gen_internet_test_timeout->setValue(Preferences::internetTimeout());

  show();
}

PreferencesWindow::~PreferencesWindow() {
  delete ui;
}

QStringList PreferencesWindow::getThemeList() {
  QStringList themeList ;

  QDirIterator directories(assetsPathFinder({}), QDir::Dirs | QDir::NoDotAndDotDot);

  while(directories.hasNext()){
    directories.next();
    themeList.append(directories.fileName());
  }

  return themeList;
}

// handle apply push button
// cancel and save are handled by accepted and rejected signals
void PreferencesWindow::buttonDispatcher(QAbstractButton * button) {
  switch(ui->buttonBox->buttonRole(button)) {
    case QDialogButtonBox::ApplyRole:
      applyClicked();
      break;
    default:
      break;
  }
}

void PreferencesWindow::applyClicked() {
  qDebug() << "apply pressed";
  updatePreferences();
}

void PreferencesWindow::updatePreferences() {
  // read prefs window values and store them
  Preferences::setTheme(ui->gen_theme->currentText());
  Preferences::setRoutingPolicy(ui->routing_policy->currentIndex());
  Preferences::setCheckIntervalAC(ui->gen_check_interval_ac->value());
  Preferences::setCheckIntervalBattery(ui->gen_check_interval_battery->value());
  Preferences::setRoutePingCount(ui->route_ping_count->value());
  Preferences::setAutomaticStart(ui->gen_automatic_start->isChecked());
  Preferences::setDnsHostname(ui->gen_dns_hostname->text());
  Preferences::setDnsTimeout(ui->gen_dns_timeout->value());
  Preferences::setCaptiveTestUrl(ui->gen_captive_test_url->text());
  Preferences::setCaptiveMaxAttempts(ui->gen_captive_test_max_attempts->value());
  Preferences::setInternetTestUrl(ui->gen_internet_test_url->text());
  Preferences::setInternetMaxAttempts(ui->gen_internet_test_max_attempts->value());
  Preferences::setInternetTimeout(ui->gen_internet_test_timeout->value());

  AutomaticStart::set(Preferences::automaticStart());

  emit preferencesChanged();
}

void PreferencesWindow::saveClicked() {
  qDebug() << "PreferencesWindow::saveClicked()";
  updatePreferences();
  close();
}
