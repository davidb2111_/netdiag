#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QString>

class Preferences {

public:
  enum routingPolicy {
    IPV4ONLY, 
    IPV6OPTIONAL,
    IPV4OPTIONAL,
    IPV4ANDIPV6,
    IPV6ONLY
  };
  Preferences();

  static QString theme();
  static uint32_t checkIntervalAC();
  static uint32_t checkIntervalBattery();
  static QString testUrl();
  static uint32_t routingPolicy();
  static uint32_t routePingCount();
  static bool automaticStart();
  static uint32_t dnsTimeout();
  static QString dnsHostname();
  static QString captiveTestUrl();
  static uint32_t captiveMaxAttempts();
  static QString internetTestUrl();
  static uint32_t internetMaxAttempts();
  static uint32_t internetTimeout();
  static void setTheme(QString);
  static void setCheckIntervalAC(uint32_t);
  static void setCheckIntervalBattery(uint32_t);
  static void setTestUrl(QString);
  static void setRoutingPolicy(uint32_t);
  static void setRoutePingCount(uint32_t);
  static void setAutomaticStart(bool);
  static void setDnsTimeout(uint32_t);
  static void setDnsHostname(QString);
  static void setCaptiveTestUrl(QString);
  static void setCaptiveMaxAttempts(uint32_t);
  static void setInternetTestUrl(QString);
  static void setInternetMaxAttempts(uint32_t);
  static void setInternetTimeout(uint32_t);
};

#endif // PREFERENCES_H
