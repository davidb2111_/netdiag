#ifndef ACMONITOR_H
#define ACMONITOR_H

#include <QObject>
#include <QTimer>

#define SYSFS_POWER_DIR "/sys/class/power_supply/"

class AcMonitor: public QObject {
  Q_OBJECT

public:
  AcMonitor();
  bool isOnline();

signals:
  void onAC();
  void onBattery();
  void powerChanged(bool);

private slots:
  void refreshStatus();

private:
  int readIntSysFile(const char *);
  QTimer * timer;
  bool m_online;
};

#endif // ACMONITOR_H
