#include "netindic.h"

#ifndef QT_NO_SYSTEMTRAYICON

#include <QAction>
#include <QCheckBox>
#include <QCloseEvent>
#include <QComboBox>
#include <QCoreApplication>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QTextEdit>
#include <QVBoxLayout>

#include "poller.h"
#include "iconTheme.h"
#include "captivePortal.h"
#include "diagnoseWindow.h"
#include "preferences.h"
#include "preferencesWindow.h"
#include "interfacesWindow.h"
#include "sessionManager.h"
#include "acMonitor.h"

#define SHOW_MODULE_NOTIFICATION 1
#define NOTIFICATION_TIMEOUT     5

Netindic::Netindic() {
  qDebug() << "Netindic::Netindic()";

  qDebug() << "Current theme" << Preferences::theme();
  iconTheme = new IconTheme(Preferences::theme());
  poller = new Poller();
  preferencesWindow = new PreferencesWindow();
  interfacesWindow = new InterfacesWindow();
  sessionManager = new SessionManager();
  acMonitor = new AcMonitor();

  setPollerInterval();

  poller->moveToThread(&workerThread);
  createActions();
  createTrayIcon();
  setIcon(STATE_EGATEWAY);
  oldState = -1;

  connect(trayIcon, &QSystemTrayIcon::messageClicked, this, &Netindic::messageClicked);
  connect(trayIcon, &QSystemTrayIcon::activated, this, &Netindic::iconActivated);
  connect(poller, SIGNAL(updated(int, QString, QString)), this, SLOT(setState(int, QString, QString)));
  connect(poller, SIGNAL(portalTrigger(bool)), this, SLOT(handleCaptivePortalWindow(bool)));
  connect(preferencesWindow, SIGNAL(preferencesChanged()), this, SLOT(preferencesChanged()));
  connect(sessionManager, SIGNAL(connectionStateUpdated(QString)), poller, SLOT(update()));
  connect(acMonitor, SIGNAL(powerChanged(bool)), this, SLOT(setPollerInterval()));

  workerThread.start();
  trayIcon->show();
  qDebug() << "starting poller";
}

Netindic::~Netindic() {
  workerThread.quit();
  workerThread.wait();
}

void Netindic::setPollerInterval() {
  if (acMonitor->isOnline())
    poller->setInterval(Preferences::checkIntervalAC());
  else
    poller->setInterval(Preferences::checkIntervalBattery());
}

void Netindic::preferencesChanged() {
  qDebug() << "Netindic::settingsChanged()";
  iconTheme = new IconTheme(Preferences::theme());
  setIcon(oldState);
  setPollerInterval();
}

void Netindic::handleCaptivePortalWindow(bool status) {
  if (captivePortalWindow.isOpen() && !status)
    captivePortalWindow.close();
  if (!captivePortalWindow.isOpen() && status) {
    captivePortalWindow.setUrl(Preferences::testUrl());
    captivePortalWindow.open();
  }
}

void Netindic::setState(int newState, QString module, QString message) {
  qDebug() << "Netindic::setState()" ;
  if (newState != oldState) {
    qDebug() << "new state" << newState;
    setIcon(newState);
    oldState = newState;
#if SHOW_MODULE_NOTIFICATION == 1
    showMessage(module, message);
#else
    showMessage(tr("Netindic"), message);
#endif
  }
}

void Netindic::setIcon(int state) {
  qDebug() << "Netindic::setIcon()" ;
  QIcon icon;
  icon.addPixmap(iconTheme->getStatePixmap(state));
  trayIcon->setIcon(icon);

//  trayIcon->setToolTip(iconComboBox->itemText(index));
}

void Netindic::iconActivated(QSystemTrayIcon::ActivationReason reason) {
  switch (reason) {
  case QSystemTrayIcon::Trigger:
  case QSystemTrayIcon::DoubleClick:
  case QSystemTrayIcon::MiddleClick:
    openDiagnoseWindow();
    break;
  default:
    ;
  }
}

void Netindic::showMessage(QString module, QString message) {
  qDebug() << "Netindic::showMessage()" ;
#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
  trayIcon->showMessage(module, message, trayIcon->icon(),
                        NOTIFICATION_TIMEOUT * 1000);
#else
  trayIcon->showMessage(module, message, QSystemTrayIcon::Information,
                        NOTIFICATION_TIMEOUT * 1000);
#endif
}

void Netindic::messageClicked() {
  QMessageBox::information(0, tr("Systray"),
                              tr("Sorry, I already gave what help I could.\n"
                                 "Maybe you should try asking a human?"));
}

void Netindic::createActions() {
  diagnoseAction = new QAction(tr("&Diagnose"), this);
  connect(diagnoseAction, &QAction::triggered, this, &Netindic::openDiagnoseWindow);

  portalAction = new QAction(tr("&Open captive portal window"), this);
  connect(portalAction, &QAction::triggered, this, &Netindic::openCaptivePortalWindow);

  interfacesAction = new QAction(tr("&Network interfaces info"), this);
  connect(interfacesAction, &QAction::triggered, this, &Netindic::openInterfacesWindow);

  preferencesAction = new QAction(tr("&Preferences"), this);
  connect(preferencesAction, &QAction::triggered, this, &Netindic::openPreferencesWindow);

  quitAction = new QAction(tr("&Quit"), this);
  connect(quitAction, &QAction::triggered, this, &Netindic::quit);
}

void Netindic::createTrayIcon() {
  trayIconMenu = new QMenu(this);
  trayIconMenu->addAction(diagnoseAction);
  trayIconMenu->addAction(portalAction);
  trayIconMenu->addAction(interfacesAction);
  trayIconMenu->addSeparator();
  trayIconMenu->addAction(preferencesAction);
  trayIconMenu->addAction(quitAction);

  trayIcon = new QSystemTrayIcon(this);
  trayIcon->setContextMenu(trayIconMenu);
}

void Netindic::openDiagnoseWindow() {
  qDebug() << "Netindic::openDiagnoseWindow()";
  diagnoseWindow.open();
}

void Netindic::openPreferencesWindow() {
  qDebug() << "Netindic::openPreferencesWindow()";
  preferencesWindow->open();
}

void Netindic::openCaptivePortalWindow() {
  qDebug() << "Netindic::openCaptivePortalWindow()";
  captivePortalWindow.setUrl(Preferences::testUrl());  
  captivePortalWindow.keepOpen();
}

void Netindic::openInterfacesWindow() {
  qDebug() << "Netindic::openInterfacesWindow()";
  interfacesWindow->open();
}

// quit menu selected
void Netindic::quit() {
  QCoreApplication::quit();
}
#endif
