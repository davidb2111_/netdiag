#ifndef POLLER_H
#define POLLER_H

#include <QDebug>
#include <QMutex>
#include <QObject>
#include <QThread>
#include <QTimer>

#include "netdiagInterfaces.h"
#include "diagnose.h"
#include "preferences.h"

class Poller : public Diagnose {
  Q_OBJECT

public:
  Poller() ;
  void setInterval(int);
public slots:
  void update();

signals:
  void updated(int, QString, QString);

private:
  QTimer * timer;
  QMutex * mutex;
  uint32_t currentCheckInterval;
  uint32_t checkInterval;
};
#endif // POLLER_H
