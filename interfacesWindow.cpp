#include <QApplication>
#include <QDebug>
#include <QDesktopWidget>
#include <QDir>
#include <QDirIterator>
#include <QHBoxLayout>
#include <QLabel>
#include <QMainWindow>
#include <QNetworkInterface>
#include <QStyle>
#include <QToolBox>
#include <QVBoxLayout>

#include "netdiagInterfaces.h"
#include "interfacesWindow.h"
#include "ui_interfaces.h"

InterfacesWindow::InterfacesWindow(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::InterfacesWindow)
{
  ui->setupUi(this);
  resize(600,400);

  int width = frameGeometry().width();
  int height = frameGeometry().height();
  QDesktopWidget wid;
  int screenWidth = wid.screen()->width();
  int screenHeight = wid.screen()->height();
  setGeometry((screenWidth/2)-(width/2),(screenHeight/2)-(height/2),width,height);
}

void InterfacesWindow::open() {
  gen_toolbox = new QToolBox(ui->frame);
  qDebug() << "InterfacesWindow::open()";

  for(QNetworkInterface interface : QNetworkInterface::allInterfaces()) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 11, 0)
    if (interface.type() == QNetworkInterface::Loopback)
      continue;
    switch(interface.type()) {
      case QNetworkInterface::Ethernet:
        gen_toolbox->addItem(forgeInterfacePage(interface), QIcon::fromTheme("network-wired"), interface.name());
        break;
      case QNetworkInterface::Wifi:
        gen_toolbox->addItem(forgeInterfacePage(interface), QIcon::fromTheme("network-wireless"), interface.name());
        break;
      default:
        gen_toolbox->addItem(forgeInterfacePage(interface), QIcon::fromTheme("computer"), interface.name());
    }
#else
    gen_toolbox->addItem(forgeInterfacePage(interface), QIcon::fromTheme("computer"), interface.name());
#endif
  }
  ui->gridLayout_2->addWidget(gen_toolbox, 0, 0, 1, 1);
  show();
}

void InterfacesWindow::closeEvent(QCloseEvent *event) {
  qDebug() << "InterfacesWindow::closeEvent()";
  ui->gridLayout->removeWidget(gen_toolbox);
  delete gen_toolbox;
  QWidget::closeEvent(event);
}

QWidget * InterfacesWindow::forgeInterfacePage(QNetworkInterface interfaceData) {
  QWidget * page = new QWidget();
  page->setGeometry(QRect(0, 0, 456, 244));

  QGridLayout * gridLayout = new QGridLayout(page);

  QSpacerItem * verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
  gridLayout->addItem(verticalSpacer, 2, 0, 1, 1);
  QSpacerItem * horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  gridLayout->addItem(horizontalSpacer, 0, 1, 1, 1);

  QLabel * pg_label_address = new QLabel(tr("IP Address(es):", "", interfaceData.addressEntries().count()), page);
  gridLayout->addWidget(pg_label_address, 0, 0, 1, 1, Qt::AlignTop);

  QLabel * pg_label_gateway = new QLabel(tr("Gateway(s):", "", findGatewayForInterface(interfaceData.name()).count()), page);
  gridLayout->addWidget(pg_label_gateway, 1, 0, 1, 1, Qt::AlignTop);

  /* network addresses
   */
  QStringList addresses;
  for(QNetworkAddressEntry address : interfaceData.addressEntries())
    addresses << address.ip().toString() + "/" + address.netmask().toString();
  
  QLabel * widget_address = new QLabel(addresses.join(QString("\n")), page);
  widget_address->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
  gridLayout->addWidget(widget_address, 0, 2, 1, 1);

  /* network gateways
   */
  QStringList gateways;
  for(QString gateway : findGatewayForInterface(interfaceData.name()))
    gateways << gateway;
  QLabel * widget_gateway = new QLabel(gateways.join(QString("\n")), page);
  widget_gateway->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
  gridLayout->addWidget(widget_gateway, 1, 2, 1, 1);


  return(page);

}

InterfacesWindow::~InterfacesWindow() {
  delete ui;
}

QStringList InterfacesWindow::findGatewayForInterface(QString interfaceName) {
  QStringList list;
  NetdiagInterfaces ndi;
  ndi.prepare();
  for(NetdiagGateway *gateway : ndi.getDefaultGatewayList())
    if (gateway->device() == interfaceName)
      list << gateway->address();

  return(list);
}
